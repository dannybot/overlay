var compressor = require('node-minify');

new compressor.minify({
    type: 'yui',
    fileIn: 'overlay.css',
    fileOut: 'public/overlay.min.css',
    callback: function(err){
        console.log(err);
    }
});

new compressor.minify({
    type: 'yui-js',
    fileIn: ['vendor/jquery.cookie.js', 'overlay.js'],
    fileOut: 'public/overlay.min.js',
    callback: function(err){
		console.log('YUI JS');
        console.log(err);
    }
});

new compressor.minify({
    type: 'yui-js',
    fileIn: ['vendor/jquery-1.8.2.min.js', 'vendor/jquery.cookie.js', 'overlay.js'],
    fileOut: 'public/overlay-jquery.min.js',
    callback: function(err){
		console.log('YUI JS');
        console.log(err);
    }
});