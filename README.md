# Design overlay

Un outil pour atteindre la perfection en terme d'intégration html et css :]

## Installation

Inclure les fichiers suivants

```
<link href="overlay/overlay.css" rel="stylesheet" >
<script src="overlay/vendor/jquery-1.8.2.min.js"></script>
<script src="overlay/vendor/jquery.cookie.js"></script>
<script src="overlay/overlay.js"></script>
```

### Code sur notre serveur

Pour sauver deux lignes de code et éviter d'avoir à télécharger les fichiers sources dans chaque projet, on peut aussi inclure les scripts sur notre serveur de dev, avec ou sans jquery d'inclus :

Sans jQuery (*si le projet contient déjà une version récente de jquery*):
```
<link href="http://code.hiboux.in/overlay/public/overlay.min.css" rel="stylesheet" >
<script src="http://code.hiboux.in/overlay/public/overlay.min.js"></script>

```

Avec jQuery :
```
<link href="http://code.hiboux.in/overlay/public/overlay.min.css" rel="stylesheet" >
<script src="http://code.hiboux.in/overlay/public/overlay-jquery.min.js"></script>

```

## Utilisation

```
$('document').ready(function(){
	$('body').overlay('relative/path/to/overlay/image.png');
});
```

On peut aussi modifier les options par défaut :

- ```left: '0px'``` : position de l'image à partir de la gauche, toujours utiliser des pixels
- ```top: '0px'``` : position de l'image à partir du haut, toujours utiliser des pixels
- ```opacity: 0.3``` : niveau de transparence de l'overlay

```
$('document').ready(function(){
	$('body').overlay({
		src: 'relative/path/to/overlay/image.png',
		left: '10px',
		top: '-4px',
		opacity: 0.2
	});
});
```




## Raccourcis claviers

- ```Shitf + o``` : Cacher / Afficher l'image en overlay
- ```Shift + flèche``` : Changer la position de l'image
- ```Shift + Alt + flèche``` : Changer la position de l'image à coup de 10px

À noter que la position de l'image est enregistrée dans un cookie, donc reste la même lorsque la page est rechargée.


## Browser support

Le code a été testé uniquement dans Chrome 22 sur Mac OS X, je n'ai pas encore pris le temps de tester dans IE7, mais ça reste dans mes plans (not).




