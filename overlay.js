// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.
;(function ( $, window, document, undefined ) {

    // undefined is used here as the undefined global
    // variable in ECMAScript 3 and is mutable (i.e. it can
    // be changed by someone else). undefined isn't really
    // being passed in so we can ensure that its value is
    // truly undefined. In ES5, undefined can no longer be
    // modified.

    // window and document are passed through as local
    // variables rather than as globals, because this (slightly)
    // quickens the resolution process and can be more
    // efficiently minified (especially when both are
    // regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = 'overlay',
      defaults = {
		    left: '0px'
		  , top: '0px'
		  , opacity: 0.3
		  , template: '<div class="designOverlay"><img src=""/></div>'
          , src: ''
      };

    // The actual plugin constructor
    function Overlay( element, options ) {
        this.element = element;

        // jQuery has an extend method that merges the
        // contents of two or more objects, storing the
        // result in the first object. The first object
        // is generally empty because we don't want to alter
        // the default options for future instances of the plugin
        this.options = typeof options == 'object' ? $.extend( {}, defaults, options) : defaults;

        if( typeof options == 'string' ){
            this.options.src = options;
        }

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Overlay.prototype.init = function () {
        // Place initialization logic here
        // You already have access to the DOM element and
        // the options via the instance, e.g. this.element
        // and this.options
        $element = $(this.element);
        $template = $(this.options.template);
        $template.find('img').attr('src', this.options.src);

        $.cookie.json = true;
        if($.cookie('overlay-' + document.URL)){
            var cookie = $.cookie('overlay-' + document.URL);
            //console.log(cookie);
            this.options.left = cookie.left;
            this.options.top = cookie.top;
            this.options.opacity = cookie.opacity;
        }

        $template.css({
            left: this.options.left,
            top: this.options.top,
            opacity: this.options.opacity,
        })

        $element.prepend($template);

        //console.log(this.options);

        var overlay = this;

        $(document).keydown(function(e){

            if(e.shiftKey && (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || e.keyCode == 79 || e.keyCode == 187 || e.keyCode == 189)){
                var overlayOn = $element.find($template).size() > 0;
                if(e.keyCode == 79){ //"o" key
                    if(overlayOn){
                        $template.remove();
                        overlayOn = false;
                    } else {
                        $element.prepend($template);
                        overlayOn = true;
                    }
                }
                if(overlayOn){
                    //position
                     var increment = e.altKey ? 10 : 1;

                    if(e.keyCode == 37){ // left arrow
                        overlay.options.left = parseFloat(overlay.options.left) - increment + 'px';
                    }else if(e.keyCode == 38){ // up arrow
                        overlay.options.top = parseFloat(overlay.options.top) - increment + 'px';
                    }else if(e.keyCode == 39){ // right arrow
                        overlay.options.left = parseFloat(overlay.options.left) + increment + 'px';
                    }else if(e.keyCode == 40){ // down arrow
                        overlay.options.top = parseFloat(overlay.options.top) + increment + 'px';
                    }

                    //opacity
                    var opacityIncrement = e.altKey ? 0.1 : 0.01;
                    var opacity = parseFloat(overlay.options.opacity);

                    if(e.keyCode == 187){ // "+" key
                        if(opacity < 1) { overlay.options.opacity += opacityIncrement; }
                    }else if(e.keyCode == 189){ // "-" key
                        if(opacity > 0) { overlay.options.opacity -= opacityIncrement; }
                    }
                }
                //console.log(overlay.options.opacity);
                //console.log(overlay);
                var css = {
                    left: overlay.options.left,
                    top: overlay.options.top,
                    opacity: overlay.options.opacity,
                }

                $template.css(css);
                $.cookie('overlay-' + document.URL, css);
            }
        })




    };


    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName,
                new Overlay( this, options ));
            }
        });
    }

})( jQuery, window, document );